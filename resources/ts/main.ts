/* eslint-disable import/order */
import '@/@iconify/icons-bundle'
import App from '@/App.vue'
import layoutsPlugin from '@/plugins/layouts'
import vuetify from '@/plugins/vuetify'
import { loadFonts } from '@/plugins/webfontloader'
import router from '@/router'
import '@core-scss/template/index.scss'
import '@styles/styles.scss'
import { createPinia } from 'pinia'
import { createApp } from 'vue'
import moshaToast from 'mosha-vue-toastify'
import 'vue3-toastify/dist/index.css'

import Vue3Toastify, {ToastContainerOptions} from "vue3-toastify";
import axios, {AxiosInstance} from 'axios';
import ApiService from "@core/services/ApiService";

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $axios: AxiosInstance;
    }
}

loadFonts()

// Create vue app
const app = createApp(App)

app.config.globalProperties.$axios = axios

// Use plugins
app.use(vuetify)
ApiService.init(app)

app.use(createPinia())
app.use(router)
app.use(moshaToast)

app.use(layoutsPlugin)

app.mount('#app')
