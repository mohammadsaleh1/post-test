import type { VerticalNavItems } from '@/@layouts/types'

export default [
  {
    title: 'Home',
    to: { name: 'index' },
    icon: { icon: 'tabler-smart-home' },
  },
  {
    title: 'Post',
    to: { name: 'post-page' },
    icon: { icon: 'tabler-file' },
  },
] as VerticalNavItems
