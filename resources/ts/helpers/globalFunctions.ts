import { toast } from "vue3-toastify"
import moment from 'moment'
import { isEmpty } from "@core/utils";
import {readonly} from "vue";
export function convertToFormData(data: Record<string, any>): FormData {
  const formData = new FormData();

  for (const key in data) {
    Array.isArray(data[key])
      ? data[key].forEach(value => formData.append(key + '[]', value))
      : formData.append(key, data[key]) ;
  }

  return formData;
}

export const showErrors = (errors: any) => {
  if(errors?.errors) {
    Object.keys(errors?.errors).forEach((list) => {
      toast.error(errors.errors[list][0])
    })
  }
  else if(errors?.message){
    toast.error(errors.message)
  }
  else {
    toast.error('Something went wrong')
  }
}
