import {acceptHMRUpdate, defineStore} from 'pinia'
import ApiService from '@core/services/ApiService'
import axios from "axios";
import {toast} from "vue3-toastify";
import {showErrors} from "@/helpers/globalFunctions";


export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    user: null,
    isAuthenticated: false,
    load: false

  }),

  getters: {
    getUser: state => () => {
      return state.user
    },
    getAuthenticated: state => () => {
      return state.isAuthenticated
    },
    getLoad: state => () => {
      return state.load
    },
  },

  actions: {

    // <!-- 👉 Get All Item   -->
    async login(data: any) {
      await axios.get(import.meta.env.VITE_BACKEND_URL + "/sanctum/csrf-cookie");
      try {
        const response: any = await axios.post(import.meta.env.VITE_BACKEND_URL + '/api/login', data)
        this.isAuthenticated = true
        this.user = response?.data?.data
        localStorage.setItem('user', JSON.stringify(response?.data?.data))
        localStorage.setItem('token', JSON.stringify(response?.data?.data))
        toast.success('User Logged in Successfully')
        return Promise.resolve(response?.data);
      } catch (error) {
        const {response} = <any>error
        toast.error(response?.data.message)
        return Promise.reject(response?.data)
      }
    },

    async signup(data: any) {
      try {
        const response: any = await ApiService.post('/signup', data)
        toast.success('User Registered Successfully')
        return Promise.resolve('')
      } catch (error) {
        const {response} = <any>error;
        showErrors(response?.data);
        return Promise.reject('')

      }
    },
    async logout() {
      try {
        const response: any = await ApiService.get('/logout')
        this.isAuthenticated = false
        this.user = null
        localStorage.removeItem('token')
        localStorage.removeItem('auth')
        localStorage.removeItem('user')
        window.location.href = '/login'
        return Promise.resolve('')
      } catch (error) {
        const {response} = <any>error;

        showErrors(response?.data);
        return Promise.reject('')

      }
    },
    async fetchUser() {
      try {
        this.load = true;
        const response: any = await ApiService.get('/user')
        this.isAuthenticated = true
        this.load = false;
        this.user = response.data
        console.log(response);
        return Promise.resolve('')
      } catch (error) {

        const {response} = <any>error;

        showErrors(response?.data);
        return Promise.reject('')

      }
    },

  },

})

if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useAuthStore, import.meta.hot))

