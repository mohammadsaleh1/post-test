import {acceptHMRUpdate, defineStore} from 'pinia'
import ApiService from '@core/services/ApiService'
import {toast} from "vue3-toastify";
import {showErrors} from "@/helpers/globalFunctions";



export interface PostData {
    id: number
    name: string
    image: string
    title: string
    category:string
    description: string
    user: object,
    userSkeleton:string
}

interface Meta {
    current_page: number
    from: number
    last_page: number
    path: string
    per_page: number
    to: number
    total: number
}

export const usePostStore = defineStore({
    id: 'post',
    state: () => ({
        items: <PostData[]>[],
        pagination: <Meta>{},
        loading:false
    }),

    getters: {
        getItems: state => () => {
            return state.items
        },
        getMeta: state => () => {
            return state.pagination
        },
        getLoader: state => () => {
            return state.loading
        },
    },

    actions: {

        // <!-- 👉 Get All Item   -->
        async fetchPosts(title: string = '', page: number = 1) {
            this.loading = true;
            return ApiService.get('/post?page=' + page + '&title=' + title).then(({data}) => {
                this.items = data.data
                this.pagination = data.meta
                this.loading = false;

                return Promise.resolve(data);
            }).catch(error => {
                this.loading = false;

                if (error?.response?.data)
                    return Promise.reject(error?.response?.data)
            })
        },
        async DeletePost(id:number) {

            return ApiService.delete('/post/'+id).then(({data}) => {
                this.items = this.items.filter((x: any, ) => x.id !== id)
              toast.success('Post deleted successfully')

                return Promise.resolve(data);
            }).catch(error => {
              const {response} = <any>error
              toast.error(response?.data.message)

                if (error?.response?.data)
                    return Promise.reject(error?.response?.data)
            })
        },

      async CreatePost(payload:any) {

        return ApiService.post('/post',payload).then(({data}) => {
          this.items.unshift(data.data);
          toast.success('Post created successfully')

          return Promise.resolve(data);
        }).catch(error => {
          const {response} = <any>error
          showErrors(response?.data);

           if (error?.response?.data)
            return Promise.reject(error?.response?.data)
        })
      },

      async UpdatePost(payload:any,id:number) {

        return ApiService.post('/post/'+id,payload).then(({data}) => {
          let index= this.items.findIndex((item)=>item.id === id);
          this.items[index] = data.data;
          toast.success('Post updated successfully')
          return Promise.resolve(data);
        }).catch(error => {
          const {response} = <any>error
          showErrors(response?.data);

          if (error?.response?.data)
            return Promise.reject(error?.response?.data)
        })
      },

    },

})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(usePostStore, import.meta.hot))

