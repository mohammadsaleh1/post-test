import {isEmpty} from "../@core/utils";

const ID_TOKEN_KEY = 'access_token' as string;

/**
 * @description get token form localStorage
 */

export const getToken = ()  => {
  const token = window.localStorage.getItem(ID_TOKEN_KEY);
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
  if(token){
    return  token.replaceAll('"' , '')
  }else {
    destroyToken()
  }

};


export const getCurrentUsers = () => {
  return window.localStorage.getItem('user');
};
export const  currentUsers = () => {
  return JSON.parse(window.localStorage.getItem('user'));
};

export const  currentUsersSpecificPermission = (name : string) => {
  const user = <any> localStorage.getItem('user') || []

  if(!isEmpty(user)){
    const json = JSON.parse(user )
    const permissions = json?.permissions || []
    return Object.fromEntries(Object.entries(permissions).filter(([key]) =>  key.includes(name)));
  }
return null
};
/**
 * @description save token into localStorage
 * @param token: string
 */
export const saveTokenLocalStorage = (token: string): void => {
  window.localStorage.setItem(ID_TOKEN_KEY, token);
};
export const saveToken = (token: string): void => {
  window.localStorage.setItem(ID_TOKEN_KEY, token);
};
export const saveUser = (payload: any ): void => {

  window.localStorage.setItem('user', payload);
};
/**
 * @description remove token form localStorage
 */
export const destroyToken = (): void => {

  window.localStorage.removeItem(ID_TOKEN_KEY);
  window.localStorage.removeItem('user');
  window.location.href = '/login'
};

export default { getToken, saveToken , saveUser, saveTokenLocalStorage, destroyToken, getCurrentUsers  , currentUsers};
