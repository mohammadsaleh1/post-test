<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait UploadFile
{
    public function getRandomFileName($destination, $ext)
    {
        $random_file_name = strtolower(Str::random(25));

        $file_name = $destination . '/' . $random_file_name . '.' . $ext;

        if (app('files')->exists($file_name)) {
            return $this->getRandomFileName($destination, $ext);
        }
        return $random_file_name . '.' . $ext;
    }


    public function uploadFile(UploadedFile $file, $destination, array $extra = null)
    {
        $extension = $file->getClientOriginalExtension();
        $photo_name = $this->getRandomFileName($destination, $extension);
        $file->move(config('filesystems.disks.public.root') . '/' . $destination, $photo_name);
        return 'storage/'.$destination . '/' . $photo_name;
    }

}
