<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = $this->user;
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'category' => $this->category,
            'user' => UserResource::make($user),
            'publication' => $this->publication,
            'image' => $this->image ? asset($this->image) :null,
            'userSkeleton'=> substr($user->name, 0, 2)
        ];
    }
}
