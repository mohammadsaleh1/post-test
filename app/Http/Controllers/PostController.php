<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Http\Resources\UserResource;
use App\Models\Post;
use App\Traits\ApiResponses;
use App\Traits\UploadFile;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;

class PostController extends Controller
{
    use UploadFile, ApiResponses;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): PostResource|array
    {
        $title = $request->title;
        $posts = Post::where('user_id', Auth::id())->
        when($title, function ($q) use ($title) {
            return $q->where('title', 'Like', '%' . $title . '%');
        })->latest()->paginate(10);

        return PostResource::collection($posts)->response()->getData(true);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PostRequest $request)
    {
        $data = $request->validated();
        $data['publication'] = Date::now();
        if ($request->hasFile('image'))
            $data['image'] = $this->uploadFile($request->image, 'posts');
        $post = Post::create($data);

        return PostResource::make($post->fresh());
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PostRequest $request, Post $post)
    {
        if ($post->user_id != Auth::id())
            return $this->forbiddenResponse('you can`t delete this post');

        $data = $request->validated();

        if ($request->hasFile('image'))
            $data['image'] = $this->uploadFile($request->image, 'posts');
        $post->update($data);
        $post->fresh();
        return PostResource::make($post);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post): JsonResponse
    {
        if ($post->user_id != Auth::id())
            return $this->forbiddenResponse('you can`t delete this post');
        $post->delete();
        return  $this->noContentResponse();
    }
}
