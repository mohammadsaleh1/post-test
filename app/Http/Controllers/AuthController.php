<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignupRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\ApiResponses;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ApiResponses;

    public function signup(SignupRequest $request)
    {
        $data = $request->validated();
        User::create($data);

        return $this->okResponse([
            'message' => 'Signup successfully'
        ]);
    }

    public function login(LoginRequest $request)
    {
        $data = $request->validated();
        if (!Auth::attempt($data)) {
            return $this->errorResponse([], 'Invalid Login Credentials');
        }
        $user = Auth::user();
        return UserResource::make($user);
    }


    public function logout(Request $request): JsonResponse
    {
        Auth::logout();

        return $this->okResponse([
            'message' => 'Logged out'
        ]);
    }
}
