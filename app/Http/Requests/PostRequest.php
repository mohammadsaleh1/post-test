<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|min:3|max:50',
            'category' => 'required|min:3|max:50',
            'image' => 'sometimes|mimes:png,jpg,jpeg|max:2048',
            'description' => 'required|min:3|max:1000',
        ];
    }

    public function validated($key = null, $default = null)
    {
        return array_merge(parent::validated(),[
            'user_id' => Auth::id()
        ]);
    }
}
