

# Project Setup
## For backend
1- install vendors 
```sh
compsoer install
```
2- create .env from .env.example

3- run migration and seed 
```sh
php artisan migrate --seed
```
4- make storage link
```sh
php artisan storage:link
```
5- 
```sh
php artisan serve
```
## For FrontEnd
1- install packages

```sh
npm install
```
2- run front end

```sh
npm run dev
```


now you can open 
``localhost:8000
``
for the first time it will take time so please be patience cause its local 
